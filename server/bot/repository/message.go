package repository

import (
	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/util"
	"time"

	"github.com/jwma/jump-jump/models"
)

type Message interface {
	SendMsg(data []byte) error
	Consume(timeout time.Duration) (*models.Message, error)
	Count() (int64, error)
	ClearList() (string, error)
}

type defaultMessage struct {
	db *redis.Client
}

func NewDefaultMessage(rdb *redis.Client) *defaultMessage {
	return &defaultMessage{rdb}
}

// SendMsg 添加内容到队列
func (r *defaultMessage) SendMsg(data []byte) error {
	// LPush 从列表左边插入数据
	return r.db.LPush(util.GetMessageKey(), data).Err()
}

// Consume 消费队列内容
func (r *defaultMessage) Consume(waitTimeout time.Duration) (*models.Message, error) {
	// 从列表的右边删除第一个数据，并返回删除的数据
	// 会阻塞线程
	result, err := r.db.BRPop(waitTimeout, util.GetMessageKey()).Result()
	if err != nil {
		return nil, err
	}

	//if len(result) < 2 {
	//	return nil, errors.New(fmt.Sprintf("内容长度异常(%d)", len(result)))
	//}

	return &models.Message{
		Topic: result[0],
		Body:  []byte(result[1]),
	}, nil
}

// Count 消费队列内容
func (r *defaultMessage) Count() (int64, error) {
	return r.db.LLen(util.GetMessageKey()).Result()
}

func (r *defaultMessage) ClearList() (string, error) {
	return r.db.LTrim(util.GetMessageKey(), 1, 0).Result()
}

//// Rem 删除列表中的数据
//func (r *defaultMessage) Rem(data []byte) error {
//
//	// LPush 从列表左边插入数据
//	return r.db.LRem(util.GetMessageKey(), data).Err()
//
// 返回从0开始到-1位置之间的数据，意思就是返回全部数据
//vals, err := client.LRange("key",0,-1).Result()
//if err != nil {
//panic(err)
//}
//fmt.Println(vals)
//
//}
