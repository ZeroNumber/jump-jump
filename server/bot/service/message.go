package service

import (
	"errors"
	"fmt"
	"github.com/jwma/jump-jump/models"
	"github.com/jwma/jump-jump/server/bot/repository"
	"github.com/jwma/jump-jump/util"
	"log"
	"os"
	"path"
	"strings"
	"time"
)

type Message interface {
	Add(data []byte) error
	Consume(waitTime time.Duration) (*models.Message, error)
	IsSetWatermark(content string) (string, bool)
	IsQueryStatus(content string) bool
	IsClearQueue(content string) bool
	SetWatermark(path string) error
	AddWatermarkToContentImage(content, watermarkPath string) (string, []string, error)
	SendMsgToClient(msgType int, uid int64, content string, sleep time.Duration) error
	GetFeedbackInfo() (int64, *time.Time, error)
	ClearQueue() error
}

const (
	PrivateMsg = iota // 0
	GroupMsg          // 1
)

type defaultMessage struct {
	repo          repository.Message
	BotCQHttpHost string
}

func NewDefaultMessage(repo repository.Message, BotCQHttpHost string) *defaultMessage {
	return &defaultMessage{
		repo:          repo,
		BotCQHttpHost: BotCQHttpHost,
	}
}

func (service *defaultMessage) Add(data []byte) error {
	return service.repo.SendMsg(data)
}

func (service *defaultMessage) Consume(waitTime time.Duration) (*models.Message, error) {
	return service.repo.Consume(waitTime)
}

func (service *defaultMessage) IsQueryStatus(content string) bool {
	if content != "查询状态" {
		return false
	}
	return true
}

func (service *defaultMessage) IsClearQueue(content string) bool {
	if content != "清空消息" {
		return false
	}
	return true
}

func (service *defaultMessage) IsSetWatermark(content string) (string, bool) {
	cqCodeData, err := util.CQCodeParse(content)
	if err != nil {
		return "", false
	}
	if len(cqCodeData) != 1 || cqCodeData[0].Type != "image" {
		return "", false
	}
	// 检测是否为闪照
	imgType, isOk := cqCodeData[0].Param["type"]
	if !isOk || imgType != "flash" {
		return "", false
	}
	filePath, isOk := cqCodeData[0].Param["file"]
	if !isOk {
		return "", false
	}
	return filePath, true
}

func (service *defaultMessage) SetWatermark(path string) error {

	// TODO: 更新水印图片
	log.Println("更新水印图片")

	//go func() {
	//	err := service.SendMsgToClient(2930929415, "设置水印成功", time.Second*3)
	//	if err != nil {
	//		log.Printf("发送反馈消息失败: %v", err)
	//	}
	//}()

	return nil
}

func (service *defaultMessage) AddWatermarkToContentImage(content, watermarkPath string) (string, []string, error) {
	cqCodeData, err := util.CQCodeParse(content)
	if err != nil {
		return "", nil, err
	}
	CQDir := "./"
	if len(cqCodeData) > 0 {
		versionInfo, err := util.CQGetVersionInfo(service.BotCQHttpHost)
		if err != nil || versionInfo.Data.CoolqDirectory == "" {
			log.Printf("获取 CQ 工作路径失败: %v", err)
			return "", nil, err
		}
		CQDir = versionInfo.Data.CoolqDirectory
	}
	// 记录生成的水印图片路径
	tmpImgPathList := make([]string, 0)
	// 消息图片加水印
	for index := range cqCodeData {
		value := cqCodeData[index]
		if value.Type != "image" {
			continue
		}
		fileID, isOK := value.Param["file"]
		if !isOK {
			continue
		}
		imgData, err := util.CQGetImageInfo(service.BotCQHttpHost, fileID)
		if err != nil {
			log.Printf("获取图片信息失败: %v", err)
			continue
		}
		// 图片加水印 (生成新文件)
		// 如果文件存在
		if _, err := os.Stat(watermarkPath); errors.Is(err, os.ErrNotExist) {
			continue
		}
		// 生成带水印图片
		tmpDirPath := os.TempDir()
		inImgAbsolutePath := path.Join(CQDir, imgData.Data.File)
		outImgName := path.Base(imgData.Data.File)
		outImgAbsolutePath := path.Join(tmpDirPath, outImgName)
		err = util.AddWatermark(inImgAbsolutePath, watermarkPath, outImgAbsolutePath)
		if err != nil {
			log.Printf("添加水印失败: %v", err)
			continue
		}
		// 替换原始图片为带水印图片
		// 并记录新生成的图片路径到列表
		content = strings.ReplaceAll(content, fileID, fmt.Sprintf("file://%s", outImgAbsolutePath))
		tmpImgPathList = append(tmpImgPathList, outImgAbsolutePath)
	}
	return content, tmpImgPathList, nil
}

func (service *defaultMessage) SendMsgToClient(msgType int, uid int64, content string, sleep time.Duration) error {
	time.Sleep(sleep)
	switch msgType {
	case PrivateMsg:
		return util.CQSendPrivateMsg(service.BotCQHttpHost, uid, content)
	case GroupMsg:
		return util.CQSendGroupMsg(service.BotCQHttpHost, uid, content)
	default:
		return fmt.Errorf("错误的消息类型")
	}
}

func (service *defaultMessage) GetFeedbackInfo() (int64, *time.Time, error) {
	msgCount, err := service.repo.Count()
	if err != nil {
		return 0, nil, err
	}
	currentTime := time.Now()
	//sleepTime := time.Second * 60 * 3
	//estimationSendTime := currentTime.Add(time.Duration(msgCount+1) * sleepTime)
	return msgCount, &currentTime, nil
}

func (service *defaultMessage) ClearQueue() error {
	_, err := service.repo.ClearList()
	if err != nil {
		return err
	}
	return nil
}

func (service *defaultMessage) sendMsgToPrivate(uid int64, content string, sleep time.Duration) error {
	time.Sleep(sleep)
	return util.CQSendPrivateMsg(service.BotCQHttpHost, uid, content)
}

func (service *defaultMessage) sendMsgToGroup(uid int64, content string, sleep time.Duration) error {
	time.Sleep(sleep)
	return util.CQSendPrivateMsg(service.BotCQHttpHost, uid, content)
}
