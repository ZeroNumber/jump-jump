package bot

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/server/bot/handler"
	"github.com/jwma/jump-jump/server/bot/repository"
	"github.com/jwma/jump-jump/server/bot/service"
	"github.com/jwma/jump-jump/util"
	"log"
	"time"
)

type server struct {
	store  *redis.Client
	router *gin.Engine
}

type ServerConfig struct {
	//BotOffWayWait     time.Duration
	//BotWorkingDayWait time.Duration
	BotCQHttpHost  string
	UID            int64
	TargetGroupUID []int64
	WatermarkPath  string
	Store          *redis.Client
}

// TODO: 待优化
var messageHandler handler.Message

func NewServer(c *ServerConfig) *server {
	// 将 GIN 设置为发布模式
	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()
	server := &server{
		store:  c.Store,
		router: router,
	}

	setupRoute(router)

	messageRepository := repository.NewDefaultMessage(c.Store)
	messageService := service.NewDefaultMessage(messageRepository, c.BotCQHttpHost)
	messageHandler = handler.NewDefaultMessage(c.UID, c.TargetGroupUID, c.WatermarkPath, messageService)
	router.POST("/", messageHandler.ReceiveAPI())

	return server
}

func (server *server) Start(addr string, botWorkingDayWait, botOffWayWait time.Duration) error {
	go func() {
		for {
			randomWait := time.Duration(util.RandomInt(0, 15))
			waitTime := botWorkingDayWait + randomWait
			if int(time.Now().Weekday()) > 5 {
				waitTime = botOffWayWait + randomWait
			}

			err := messageHandler.SendApi(waitTime)
			if err != nil {
				log.Printf("%#v", err)
			}
		}
	}()

	return server.router.Run(addr)
}

func setupRoute(r *gin.Engine) {
	if gin.Mode() == gin.DebugMode { // 开发环境下，开启 CORS
		corsCfg := cors.DefaultConfig()
		corsCfg.AllowAllOrigins = true
		corsCfg.AddAllowHeaders("Authorization")
		r.Use(cors.New(corsCfg))
	}

	r.Use(handler.AllowedHostsMiddleware())
}
