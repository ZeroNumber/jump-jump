package handler

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jwma/jump-jump/server/bot/service"
	"github.com/jwma/jump-jump/util"
	"log"
	"net/http"
	"os"
	"time"
)

type Message interface {
	ReceiveAPI() gin.HandlerFunc
	SendApi(sleepTime time.Duration) error
}

type defaultMessage struct {
	uid            int64   // 管理员号码
	targetGroupUID []int64 // 目标群号码
	watermarkPath  string  //  水印图片路径
	isWait         bool    // 是否有已取出待发送的消息
	service        service.Message
}

func NewDefaultMessage(uid int64, targetGroupUID []int64, watermarkPath string, ms service.Message) *defaultMessage {
	return &defaultMessage{uid: uid, targetGroupUID: targetGroupUID, watermarkPath: watermarkPath, service: ms}
}

type ReceiveData struct {
	Anonymous   interface{} `json:"anonymous"`
	Font        int         `json:"font"`
	GroupID     int         `json:"group_id"`
	Message     string      `json:"message"`
	MessageID   int         `json:"message_id"`
	MessageSeq  int         `json:"message_seq"`
	MessageType string      `json:"message_type"` // binding:"required"
	PostType    string      `json:"post_type"`
	RawMessage  string      `json:"raw_message"` // binding:"required"
	SelfID      int         `json:"self_id"`
	Sender      struct {
		Age      int    `json:"age"`
		Area     string `json:"area"`
		Card     string `json:"card"`
		Level    string `json:"level"`
		Nickname string `json:"nickname"`
		Role     string `json:"role"`
		Sex      string `json:"sex"`
		Title    string `json:"title"`
		UserID   int    `json:"user_id"` // binding:"required"
	} `json:"sender"`
	SubType string `json:"sub_type"`
	UserID  int64  `json:"user_id"`
	//Time    int    `json:"time"`
}

// ReceiveAPI 接收消息
func (handler *defaultMessage) ReceiveAPI() gin.HandlerFunc {
	return func(c *gin.Context) {
		params := &ReceiveData{}
		if err := c.ShouldBindJSON(&params); err != nil {
			log.Println(err)
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		// 过滤非私聊消息
		if params.MessageType != "private" || params.UserID != handler.uid {
			c.AbortWithStatus(http.StatusOK)
			return
		}

		switch {
		case handler.service.IsClearQueue(params.RawMessage):
			err := handler.service.ClearQueue()
			if err != nil {
				log.Println(err)
				c.AbortWithStatus(http.StatusBadRequest)
				return
			}
			fallthrough
		case handler.service.IsQueryStatus(params.RawMessage):
			waitQueue, currentTime, err := handler.service.GetFeedbackInfo()
			if err != nil {
				log.Println(err)
				c.AbortWithStatus(http.StatusBadRequest)
				return
			}
			// 如果有已取出消息
			// 消息计数 +1
			waitSend := 0
			if handler.isWait {
				waitSend = 1
			}
			// 发送反馈信息
			go func() {
				content := fmt.Sprintf("管理账号: %v\n-----------\n当前时间: %v\n-----------\n等待发送: %v 条\n-----------\n即将发送: %v 条\n-----------\n目标群组: %v",
					handler.uid, currentTime.Format("1月2日 15:04"), waitQueue, waitSend, handler.targetGroupUID)
				sleepTime := time.Second * time.Duration(util.RandomInt(1, 5))
				err := handler.service.SendMsgToClient(service.PrivateMsg, handler.uid, content, sleepTime)
				if err != nil {
					log.Printf("发送反馈消息失败: %v", err)
				}
			}()
		default:
			err := handler.service.Add([]byte(params.RawMessage))
			if err != nil {
				log.Println(err)
				c.AbortWithStatus(http.StatusBadRequest)
				return
			}
		}

		c.Status(http.StatusOK)
	}
}

func (handler *defaultMessage) SendApi(sleep time.Duration) error {
	consume, err := handler.service.Consume(0)
	if err != nil {
		return err
	}

	// 更新等待状态
	handler.isWait = true
	defer func() {
		handler.isWait = false
	}()

	// 等待间隔时间
	time.Sleep(sleep)

	// 为内容中的图片添加水印
	// 图片分辨率越高耗时越旧
	msg, tmpWatermarkImg, err := handler.service.AddWatermarkToContentImage(string(consume.Body), handler.watermarkPath)
	if err != nil {
		return err
	}

	for _, groupUID := range handler.targetGroupUID {
		err = handler.service.SendMsgToClient(service.GroupMsg, groupUID, msg, time.Second*3)
		if err != nil {
			log.Printf("发送消息到群组(%v)失败: %v", groupUID, err)
			continue
		}
	}

	// 删除临时图片
	go func(paths []string) {
		for index := range paths {
			imgPath := tmpWatermarkImg[index]
			if err := os.Remove(imgPath); err != nil {
				log.Println(err)
			}
		}
	}(tmpWatermarkImg)

	return nil
}
