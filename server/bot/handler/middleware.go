package handler

import (
	"fmt"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/thoas/go-funk"

	"github.com/jwma/jump-jump/internal/app"
)

// AllowedHostsMiddleware
// 检查当前请求的 Host 是否属于我们所设定的 Host 列表中的其中一个
// 如果不在设定列表中，则返回 HTTP Code 400 并中断后续逻辑的处理
func AllowedHostsMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		if app.AllowedHosts != "" && app.AllowedHosts != "*" {
			h := strings.Split(c.Request.Host, ":")[0]

			if !funk.ContainsString(strings.Split(app.AllowedHosts, ","), h) {
				output := ""

				if gin.Mode() == gin.DebugMode {
					output = fmt.Sprintf("You can see this message because GIN_MODE=debug.\n"+
						"Invalid HTTP_HOST header: '%s'. "+
						"You may need to add '%s' to ALLOWED_HOSTS environment variable.", c.Request.Host, h)
				}

				c.String(http.StatusBadRequest, output)
				c.Abort()
				return
			}
		}
	}
}
