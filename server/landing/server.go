package landing

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"

	"github.com/jwma/jump-jump/internal/app/config"
	"github.com/jwma/jump-jump/internal/app/repository"
	"github.com/jwma/jump-jump/models"
)

type server struct {
	store  *redis.Client
	router *gin.Engine
}

func NewServer(store *redis.Client) *server {
	router := gin.Default()
	server := &server{
		store:  store,
		router: router,
	}

	// 创建路由
	// 并注册处理器
	//router := gin.Default()
	//r.GET("/", handlers.LandingHome)
	router.GET("/:id", server.Redirect)
	//server.router = router

	return server
}

func (server *server) Start(addr string) error {
	return server.router.Run(addr)
}

func (server *server) Redirect(c *gin.Context) {
	if c.Param("id") == "favicon.ico" {
		c.String(http.StatusNotFound, "")
		return
	}

	slRepo := repository.NewShortLinkRepo(server.store)
	s, err := slRepo.Get(c.Param("id"))

	if err != nil {
		log.Printf("查找短链接失败, error: %v\n", err)
		cc := config.GetShortLinkNotFoundConfig()

		switch cc.Mode {
		case config.ShortLinkNotFoundContentMode:
			c.String(http.StatusOK, cc.Value)
		case config.ShortLinkNotFoundRedirectMode:
			c.Redirect(http.StatusTemporaryRedirect, cc.Value)
		default:
			c.String(http.StatusOK, "你访问的页面不存在哦")
		}

		return
	}

	if !s.IsEnable {
		c.String(http.StatusOK, "你访问的页面不存在哦")
		return
	}

	// 保存短链接请求记录（IP、User-Agent），保存活跃链接记录
	rhRepo := repository.NewRequestHistoryRepo(server.store)
	alRepo := repository.NewActiveLinkRepo(server.store)
	go func() {
		rhRepo.Save(models.NewRequestHistory(s, c.ClientIP(), c.Request.UserAgent()))
		alRepo.Save(s.Id)
	}()

	c.Redirect(http.StatusTemporaryRedirect, s.Url)
}
