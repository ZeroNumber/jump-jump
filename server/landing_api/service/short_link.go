package service

import (
	"github.com/jwma/jump-jump/models"
	"github.com/jwma/jump-jump/server/landing_api/repository"
	"time"
)

type ShortLink interface {
	Get(id string) (*models.ShortLink, error)
	Update(s *models.ShortLink, url string, desc string, isEnable bool) error
	Delete(s *models.ShortLink)
	List(key string, start int64, stop int64) (*repository.ShortLinkListResult, error)
	GenerateId(l int) (string, error)
	Save(s *models.ShortLink) error
	FindByDateRange(linkId string, startTime, endTime time.Time) []*models.RequestHistory
}

type defaultShortLink struct {
	repo        repository.ShortLink
	historyRepo repository.RequestHistory
}

func NewDefaultShortLink(repo repository.ShortLink, historyRepo repository.RequestHistory) *defaultShortLink {
	return &defaultShortLink{
		repo:        repo,
		historyRepo: historyRepo,
	}
}

func (service *defaultShortLink) Get(id string) (*models.ShortLink, error) {
	return service.repo.Get(id)
}

func (service *defaultShortLink) Update(s *models.ShortLink, url string, desc string, isEnable bool) error {
	return service.repo.Update(s, url, desc, isEnable)
}

func (service *defaultShortLink) Delete(s *models.ShortLink) {
	service.repo.Delete(s)
}

func (service *defaultShortLink) List(key string, start int64, stop int64) (*repository.ShortLinkListResult, error) {
	return service.repo.List(key, start, stop)
}

func (service *defaultShortLink) GenerateId(l int) (string, error) {
	return service.repo.GenerateId(l)
}

func (service *defaultShortLink) Save(s *models.ShortLink) error {
	return service.repo.Save(s)
}

func (service *defaultShortLink) FindByDateRange(linkId string, startTime, endTime time.Time) []*models.RequestHistory {
	return service.historyRepo.FindByDateRange(linkId, startTime, endTime)
}
