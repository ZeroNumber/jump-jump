package service

import (
	"github.com/jwma/jump-jump/models"
	"github.com/jwma/jump-jump/server/landing_api/repository"
)

type User interface {
	FindOneByUsername(username string) (*models.User, error)
	UpdatePassword(u *models.User) error
}

type defaultUser struct {
	repo repository.User
}

func NewDefaultUser(repo repository.User) *defaultUser {
	return &defaultUser{repo: repo}
}

func (service *defaultUser) FindOneByUsername(username string) (*models.User, error) {
	return service.repo.FindOneByUsername(username)
}

func (service *defaultUser) UpdatePassword(u *models.User) error {
	return service.repo.UpdatePassword(u)
}
