package repository

import (
	"github.com/jwma/jump-jump/models"
)

type ShortLinkListResult struct {
	ShortLinks []*models.ShortLink `json:"shortLinks"`
	Total      int64               `json:"total"`
}

func makeEmptyShortLinkListResult() *ShortLinkListResult {
	return &ShortLinkListResult{
		ShortLinks: make([]*models.ShortLink, 0),
		Total:      0,
	}
}

func (r *ShortLinkListResult) addLink(links ...*models.ShortLink) {
	r.ShortLinks = append(r.ShortLinks, links...)
}

type defaultRequestHistoryListResult struct {
	Histories []*models.RequestHistory `json:"histories"`
	Total     int                      `json:"total"`
}

func newEmptyRequestHistoryResult() *defaultRequestHistoryListResult {
	return &defaultRequestHistoryListResult{
		Histories: make([]*models.RequestHistory, 0),
		Total:     0,
	}
}

func (r *defaultRequestHistoryListResult) addHistory(h ...*models.RequestHistory) {
	r.Histories = append(r.Histories, h...)
	r.Total = len(r.Histories)
}
