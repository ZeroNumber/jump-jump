package repository

import (
	"github.com/jwma/jump-jump/util"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/models"
)

type ActiveLink interface {
	Save(linkId string)
	FindByDateRange(startTime, endTime time.Time) []*models.ActiveLink
}

type defaultActiveLinkRepository struct {
	db *redis.Client
}

func NewDefaultActiveLinkRepo(rdb *redis.Client) *defaultActiveLinkRepository {
	return &defaultActiveLinkRepository{rdb}
}

func (r *defaultActiveLinkRepository) Save(linkId string) {
	r.db.ZAdd(util.GetActiveLinkKey(), redis.Z{
		Score:  float64(time.Now().Unix()),
		Member: linkId,
	})
}

func (r *defaultActiveLinkRepository) FindByDateRange(startTime, endTime time.Time) []*models.ActiveLink {
	result := make([]*models.ActiveLink, 0)
	rs, _ := r.db.ZRangeByScoreWithScores(util.GetActiveLinkKey(), redis.ZRangeBy{
		Min: strconv.Itoa(int(startTime.Unix())),
		Max: strconv.Itoa(int(endTime.Unix())),
	}).Result()

	for _, one := range rs {
		result = append(result, &models.ActiveLink{Id: one.Member.(string), Time: time.Unix(int64(one.Score), 0)})
	}

	return result
}
