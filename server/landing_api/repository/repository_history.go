package repository

import (
	"encoding/json"
	"github.com/jwma/jump-jump/util"
	"log"
	"strconv"
	"time"

	"github.com/go-redis/redis"

	"github.com/jwma/jump-jump/models"
)

type RequestHistory interface {
	Save(rh *models.RequestHistory)
	FindLatest(linkId string, size int64) (*defaultRequestHistoryListResult, error)
	FindByDateRange(linkId string, startTime, endTime time.Time) []*models.RequestHistory
}

type defaultRequestHistoryRepository struct {
	db *redis.Client
}

func NewDefaultRequestHistory(rdb *redis.Client) *defaultRequestHistoryRepository {
	return &defaultRequestHistoryRepository{rdb}
}

func (r *defaultRequestHistoryRepository) Save(rh *models.RequestHistory) {
	rh.Id = util.RandStringRunes(6)
	rh.Time = time.Now()
	key := util.GetRequestHistoryKey(rh.Link.Id)

	_, err := r.db.ZAdd(key, redis.Z{
		Score:  float64(rh.Time.Unix()),
		Member: rh,
	}).Result()

	if err != nil {
		log.Printf("fail to save request history with key: %s, error: %v, data: %v\n", key, err, rh)
	}
}

func (r *defaultRequestHistoryRepository) FindLatest(linkId string, size int64) (*defaultRequestHistoryListResult, error) {
	key := util.GetRequestHistoryKey(linkId)
	rs, err := r.db.ZRangeWithScores(key, -size, -1).Result()

	if err != nil {
		log.Printf("failed to find request history latest records with key: %s, err: %v\n", key, err)
	}

	util.ReverseAny(rs)
	result := newEmptyRequestHistoryResult()
	for _, one := range rs {
		rh := &models.RequestHistory{}
		_ = json.Unmarshal([]byte(one.Member.(string)), rh)
		result.addHistory(rh)
	}

	return result, nil
}

func (r *defaultRequestHistoryRepository) FindByDateRange(linkId string, startTime, endTime time.Time) []*models.RequestHistory {
	rs, _ := r.db.ZRangeByScoreWithScores(util.GetRequestHistoryKey(linkId), redis.ZRangeBy{
		Min: strconv.Itoa(int(startTime.Unix())),
		Max: strconv.Itoa(int(endTime.Unix())),
	}).Result()
	rhs := make([]*models.RequestHistory, 0)

	for _, one := range rs {
		rh := &models.RequestHistory{}
		_ = json.Unmarshal([]byte(one.Member.(string)), rh)

		rhs = append(rhs, rh)
	}

	return rhs
}
