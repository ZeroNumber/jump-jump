package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jwma/jump-jump/util"
	"log"
	"time"

	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/models"
)

type ShortLink interface {
	GenerateId(l int) (string, error)
	Save(s *models.ShortLink) error
	Update(s *models.ShortLink, url string, desc string, isEnable bool) error
	Delete(s *models.ShortLink)
	Get(id string) (*models.ShortLink, error)
	List(key string, start int64, stop int64) (*ShortLinkListResult, error)
}

type defaultShortLinkRepository struct {
	db *redis.Client
}

func NewDefaultShortLinkRepo(rdb *redis.Client) *defaultShortLinkRepository {
	return &defaultShortLinkRepository{rdb}
}

func (r *defaultShortLinkRepository) GenerateId(l int) (string, error) {
	var id string

	for {
		id = util.RandStringRunes(l)
		rs, err := r.db.Exists(util.GetShortLinkKey(id)).Result()

		if rs == 0 {
			break
		}

		if err != nil {
			log.Println(err)
			return "", err
		}
	}

	return id, nil
}

func (r *defaultShortLinkRepository) Save(s *models.ShortLink) error {
	return r.save(s, false)
}

func (r *defaultShortLinkRepository) Update(s *models.ShortLink, url string, desc string, isEnable bool) error {
	s.Url = url
	s.Description = desc
	s.IsEnable = isEnable

	return r.save(s, true)
}

func (r *defaultShortLinkRepository) Delete(s *models.ShortLink) {
	pipeline := r.db.Pipeline()

	// 删除短链接本身
	pipeline.Del(util.GetShortLinkKey(s.Id))
	// 删除用户的短链接记录及全局短链接记录
	pipeline.ZRem(util.GetUserShortLinksKey(s.CreatedBy), s.Id)
	pipeline.ZRem(util.GetShortLinksKey(), s.Id)
	_, _ = pipeline.Exec()

	// 删除访问历史和报表
	r.db.Del(util.GetRequestHistoryKey(s.Id))
	r.db.Del(util.GetDailyReportKey(s.Id))
}

func (r *defaultShortLinkRepository) Get(id string) (*models.ShortLink, error) {
	if id == "" {
		return nil, fmt.Errorf("短链接不存在")
	}

	key := util.GetShortLinkKey(id)
	s := &models.ShortLink{}
	rs, err := r.db.Get(key).Result()
	if err != nil {
		log.Printf("fail to get short Link with Key: %s, error: %v\n", key, err)
		return s, fmt.Errorf("短链接不存在")
	}

	err = json.Unmarshal([]byte(rs), s)
	if err != nil {
		log.Printf("fail to unmarshal short Link, Key: %s, error: %v\n", key, err)
		return s, fmt.Errorf("短链接不存在")
	}

	return s, nil
}

func (r *defaultShortLinkRepository) List(key string, start int64, stop int64) (*ShortLinkListResult, error) {
	result := makeEmptyShortLinkListResult()

	total, _ := r.db.ZCard(key).Result()
	result.Total = total
	if total == 0 {
		return result, nil
	}

	ids, err := r.db.ZRevRange(key, start, stop).Result()
	if err != nil {
		return result, errors.New("系统繁忙请稍后再试")
	}

	if len(ids) == 0 {
		return result, nil
	}

	linkRs := make([]*redis.StringCmd, 0)
	pipeline := r.db.Pipeline()
	for _, id := range ids {
		r := pipeline.Get(util.GetShortLinkKey(id))
		linkRs = append(linkRs, r)
	}
	_, _ = pipeline.Exec()

	for _, cmd := range linkRs {
		s := &models.ShortLink{}
		err = json.Unmarshal([]byte(cmd.Val()), s)
		if err != nil {
			return nil, err
		}
		result.addLink(s)
	}
	return result, nil
}

func (r *defaultShortLinkRepository) save(s *models.ShortLink, isUpdate bool) error {
	if isUpdate && s.Id == "" {
		return fmt.Errorf("id错误")
	}
	if s.Url == "" {
		return fmt.Errorf("请填写url")
	}
	if s.CreatedBy == "" {
		return fmt.Errorf("未设置创建者，请通过接口创建短链接")
	}

	if !isUpdate {
		s.CreateTime = time.Now()
	}

	s.UpdateTime = time.Now()
	j, _ := json.Marshal(s)

	pipeline := r.db.Pipeline()
	// 保存短链接
	pipeline.Set(util.GetShortLinkKey(s.Id), j, 0)
	// 保存用户的短链接记录，保存到创建者及全局
	record := redis.Z{
		Score:  float64(time.Now().Unix()),
		Member: s.Id,
	}
	pipeline.ZAdd(util.GetUserShortLinksKey(s.CreatedBy), record)
	pipeline.ZAdd(util.GetShortLinksKey(), record)
	_, err := pipeline.Exec()

	if err != nil {
		log.Println(err)
		return errors.New("服务器繁忙，请稍后再试")
	}

	return nil
}
