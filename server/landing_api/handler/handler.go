package handler

import "github.com/gin-gonic/gin"

func errorResponse(code int, msg string) gin.H {
	return gin.H{"msg": msg, "code": code, "data": nil}
}

func successResponse(data interface{}) gin.H {
	return gin.H{"msg": "success", "code": 0, "data": data}
}
