package handler

import (
	"fmt"
	"github.com/jwma/jump-jump/util"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/jwma/jump-jump/internal/app/config"
	"github.com/jwma/jump-jump/models"
	"github.com/jwma/jump-jump/server/landing_api/service"
)

type ShortLink interface {
	GetShortLinkAPI() gin.HandlerFunc
	CreateShortLinkAPI() gin.HandlerFunc
	UpdateShortLinkAPI() gin.HandlerFunc
	DeleteShortLinkAPI() gin.HandlerFunc
	DeleteShortLinkAPIV2() gin.HandlerFunc
	ListShortLinksAPI() gin.HandlerFunc
	ShortLinkActionAPI() gin.HandlerFunc
}

type defaultShortLink struct {
	service service.ShortLink
}

func NewDefaultShortLink(ms service.ShortLink) *defaultShortLink {
	return &defaultShortLink{service: ms}
}

type GetShortLinkAPIResponseData struct {
	ShortLinkData *models.ShortLinkData `json:"shortLink"`
} // @name GetShortLinkAPIResponseData

// GetShortLinkAPI godoc
// @Security ApiKeyAuth
// @Summary 获取指定 ID 短链接
// @Description 获取系统配置信息
// @Tags 短链接
// @Accept json
// @Produce json
// @Param id path string true "短链接 ID"
// @Success 200 {object} models.Response{data=models.GetShortLinkAPIResponseData}
// @Failure 401
// @Router /short-link/{id} [get]
func (handler *defaultShortLink) GetShortLinkAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		//slRepo := repository.NewShortLinkRepo(server.store)
		//s, err := slRepo.Get(c.Param("id"))
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		s, err := handler.service.Get(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		if !user.IsAdmin() && user.Username != s.CreatedBy {
			c.JSON(http.StatusOK, errorResponse(4999, "你无权查看"))
			return
		}

		c.JSON(http.StatusOK, successResponse(&GetShortLinkAPIResponseData{
			ShortLinkData: models.ToShortLinkData(s),
		}))
	})
}

type CreateShortLinkAPIRequest struct {
	// 只有管理员可以在创建的时候指定 ID
	Id string `json:"id" format:"string" example:"RANDOM_ID"`
	// 目标链接
	Url string `json:"url" example:"https://github.com/jwma/jump-jump"`
	// 描述
	Description string `json:"description" example:"Jump Jump project"`
	// 是否启用
	IsEnable bool `json:"isEnable" example:"true" format:"boolean"`
	// 短链接 ID 长度
	IdLength int `json:"idLength" example:"4" format:"int"`
} // @name CreateShortLinkAPIRequest

type CreateShortLinkAPIResponseData struct {
	ShortLinkData *models.ShortLinkData `json:"shortLink"`
} // @name CreateShortLinkAPIResponseData

func NewShortLink(createdBy string, r *CreateShortLinkAPIRequest) *models.ShortLink {
	return &models.ShortLink{
		Id:          r.Id,
		Url:         r.Url,
		Description: r.Description,
		IsEnable:    r.IsEnable,
		CreatedBy:   createdBy,
	}
}

// CreateShortLinkAPI godoc
// @Security ApiKeyAuth
// @Summary 创建短链接
// @Description 创建短链接
// @Tags 短链接
// @Accept json
// @Produce json
// @Param body body models.CreateShortLinkAPIRequest true "创建短链接请求"
// @Success 200 {object} models.Response{data=models.CreateShortLinkAPIResponseData}
// @Failure 401
// @Router /short-link/ [post]
func (handler *defaultShortLink) CreateShortLinkAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		var err error
		params := &CreateShortLinkAPIRequest{}

		if err := c.ShouldBindJSON(&params); err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, "参数错误"))
			return
		}

		s := NewShortLink(user.Username, params)
		//repo := repository.NewShortLinkRepo(server.store)
		idCfg := config.GetIdConfig()
		idLen := idCfg.IdLength

		if user.Role == models.RoleUser {
			s.Id = "" // 如果是普通用户，创建时不可以指定 ID
		}

		if s.Id != "" {
			// 如果管理员指定了 ID，则检查 ID 是否可用
			checkShortLink, _ := handler.service.Get(s.Id)

			if checkShortLink.Id != "" {
				c.JSON(http.StatusOK, errorResponse(4999, fmt.Sprintf("%s 已被占用，请使用其他 ID。", s.Id)))
				return
			}
		} else {
			// 如果管理员没有指定 ID，则计算随机 ID 的长度
			if idCfg.IdMinimumLength <= params.IdLength && params.IdLength <= idCfg.IdMaximumLength { // 检查是否在合法的范围内
				idLen = params.IdLength
			}

			id, err := handler.service.GenerateId(idLen) // 生成指定长度的随机 ID

			if err != nil {
				log.Printf("generate id failed, error: %v\n", err)
				c.JSON(http.StatusOK, errorResponse(4999, "服务器繁忙，请稍后再试"))
				return
			}

			s.Id = util.TrimShortLinkId(id)
		}

		if s.Id == "" {
			log.Println("短链接 ID 为空")
			c.JSON(http.StatusOK, errorResponse(4999, "ID 错误"))
		}

		err = handler.service.Save(s)
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		c.JSON(http.StatusOK, successResponse(CreateShortLinkAPIResponseData{
			ShortLinkData: models.ToShortLinkData(s),
		}))
	})
}

type UpdateShortLinkAPIResponseData struct {
	ShortLinkData *models.ShortLinkData `json:"shortLink"`
} // @name UpdateShortLinkAPIResponseData

type UpdateShortLinkAPIRequest struct {
	// 目标链接
	Url string `json:"url" binding:"required" example:"https://github.com/jwma/jump-jump"`
	// 描述
	Description string `json:"description" example:"Jump Jump project"`
	// 是否启用
	IsEnable bool `json:"isEnable" example:"true" format:"boolean"`
} // @name UpdateShortLinkAPIRequest

// UpdateShortLinkAPI godoc
// @Security ApiKeyAuth
// @Summary 更新短链接
// @Description 更新短链接
// @Tags 短链接
// @Accept json
// @Produce json
// @Param id path string true "短链接 ID"
// @Param body body models.UpdateShortLinkAPIRequest true "更新短链接请求"
// @Success 200 {object} models.Response{data=models.UpdateShortLinkAPIResponseData}
// @Failure 401
// @Router /short-link/{id} [patch]
func (handler *defaultShortLink) UpdateShortLinkAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		//slRepo := repository.NewShortLinkRepo(server.store)
		//s, err := slRepo.Get(c.Param("id"))
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		s, err := handler.service.Get(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		if !user.IsAdmin() && user.Username != s.CreatedBy {
			c.JSON(http.StatusOK, errorResponse(4999, "你无权修改此短链接"))
			return
		}

		updateShortLink := &UpdateShortLinkAPIRequest{}
		if err := c.ShouldBindJSON(updateShortLink); err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		//repo := repository.NewShortLinkRepo(server.store)
		//err = repo.Update(s, updateShortLink.Url, updateShortLink.Description, updateShortLink.IsEnable)
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		err = handler.service.Update(s, updateShortLink.Url, updateShortLink.Description, updateShortLink.IsEnable)
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		c.JSON(http.StatusOK, successResponse(UpdateShortLinkAPIResponseData{
			ShortLinkData: models.ToShortLinkData(s),
		}))
	})
}

// DeleteShortLinkAPI godoc
// @Security ApiKeyAuth
// @Summary 删除短链接
// @Description 删除短链接
// @Tags 短链接
// @Accept json
// @Produce json
// @Param id path string true "短链接 ID"
// @Success 200 {object} models.Response
// @Failure 401
// @Router /short-link/{id} [delete]
func (handler *defaultShortLink) DeleteShortLinkAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		//slRepo := repository.NewShortLinkRepo(server.store)
		//s, err := slRepo.Get(c.Param("id"))
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		s, err := handler.service.Get(c.Param("id"))
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		if !user.IsAdmin() && user.Username != s.CreatedBy {
			c.JSON(http.StatusOK, errorResponse(4999, "你无权删除此短链接"))
			return
		}

		//repo := repository.NewShortLinkRepo(server.store)
		//repo.Delete(s)
		handler.service.Delete(s)

		c.JSON(http.StatusOK, successResponse(nil))
	})
}

type DeleteShortLinkAPIRequest struct {
	// 只有管理员可以在创建的时候指定 ID
	Id string `json:"id" format:"string" example:"RANDOM_ID"`
} // @name DelectShortLinkAPIRequest

// DeleteShortLinkAPIV2 godoc
// @Security ApiKeyAuth
// @Summary 删除短链接
// @Description 删除短链接
// @Tags 短链接
// @Accept json
// @Produce json
// @Param body body models.DeleteShortLinkAPIRequest true "短链接 ID"
// @Success 200 {object} models.Response{data=models.DeleteShortLinkAPIResponseData}
// @Failure 401
// @Router /v2/short-link/ [delete]
func (handler *defaultShortLink) DeleteShortLinkAPIV2() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		var err error
		params := &DeleteShortLinkAPIRequest{}

		if err := c.ShouldBindJSON(&params); err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, "参数错误"))
			return
		}

		//slRepo := repository.NewShortLinkRepo(server.store)
		//s, err := slRepo.Get(params.Id)
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		s, err := handler.service.Get(params.Id)
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		if !user.IsAdmin() && user.Username != s.CreatedBy {
			c.JSON(http.StatusOK, errorResponse(4999, "你无权删除此短链接"))
			return
		}

		//repo := repository.NewShortLinkRepo(server.store)
		//repo.Delete(s)
		handler.service.Delete(s)

		c.JSON(http.StatusOK, successResponse(nil))
	})
}

type ListShortLinksAPIResponseData struct {
	ShortLinks []*models.ShortLinkData `json:"shortLinks"`
	Total      int64                   `json:"total" example:"10" format:"10"`
} // @name ListShortLinksAPIResponseData

// ListShortLinksAPI godoc
// @Security ApiKeyAuth
// @Summary 短链接列表
// @Description 短链接列表
// @Tags 短链接
// @Accept json
// @Produce json
// @Param page query int false "页码"
// @Param pageSize query int false "每页条数"
// @Success 200 {object} models.Response{data=models.ListShortLinksAPIResponseData}
// @Failure 401
// @Router /short-link/ [get]
func (handler *defaultShortLink) ListShortLinksAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		var page = util.GetIntQueryValue(c, "page", 1)
		var pageSize = util.GetIntQueryValue(c, "pageSize", 20)
		start := int64((page - 1) * pageSize)
		stop := start - 1 + int64(pageSize)

		var key string
		if user.IsAdmin() {
			key = util.GetShortLinksKey()
		} else {
			key = util.GetUserShortLinksKey(user.Username)
		}

		//slRepo := repository.NewShortLinkRepo(server.store)
		//result, err := slRepo.List(key, start, stop)
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		result, err := handler.service.List(key, start, stop)
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		c.JSON(http.StatusOK, successResponse(&ListShortLinksAPIResponseData{
			ShortLinks: models.ToShortLinkDataSlice(result.ShortLinks),
			Total:      result.Total,
		}))
	})
}

type ShortLinkDataAPIResponseData struct {
	Histories []*models.RequestHistory `json:"histories"`
} // @name ShortLinkDataAPIResponseData

// ShortLinkActionAPI godoc
// @Security ApiKeyAuth
// @Summary 短链接访问数据
// @Description 可查询短链接某个日期范围内的访问数据
// @Tags 短链接
// @Accept json
// @Produce json
// @Param id path string true "短链接 ID"
// @Param startDate query string true "开始日期 YYYY-mm-dd"
// @Param endDate query string true "结束日期 YYYY-mm-dd"
// @Success 200 {object} models.Response{data=models.ShortLinkDataAPIResponseData}
// @Failure 401
// @Router /short-link/{id}/data [get]
func (handler *defaultShortLink) ShortLinkActionAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {

		if c.Param("action") == "/data" {
			//slRepo := repository.NewShortLinkRepo(server.store)
			//s, err := slRepo.Get(c.Param("id"))
			//if err != nil {
			//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			//	return
			//}
			s, err := handler.service.Get(c.Param("id"))
			if err != nil {
				c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
				return
			}

			if !user.IsAdmin() && user.Username != s.CreatedBy {
				c.JSON(http.StatusOK, errorResponse(4999, "你无权查看"))
				return
			}

			startDate := c.Query("startDate")
			endDate := c.Query("endDate")

			if startDate == "" || endDate == "" {
				c.JSON(http.StatusOK, errorResponse(4999, "参数错误"))
				return
			}

			startTime, err := time.ParseInLocation("2006-01-02", startDate, time.Local)
			if err != nil {
				c.JSON(http.StatusOK, errorResponse(4999, "开始日期参数错误"))
				return
			}

			endTime, err := time.ParseInLocation("2006-01-02", endDate, time.Local)
			if err != nil {
				c.JSON(http.StatusOK, errorResponse(4999, "结束日期参数错误"))
				return
			}

			endTime = time.Date(endTime.Year(), endTime.Month(), endTime.Day(), 23, 59, 59, 0, time.Local)
			//rhRepo := repository.NewRequestHistoryRepo(server.store)
			//rhs := rhRepo.FindByDateRange(s.Id, startTime, endTime)
			rhs := handler.service.FindByDateRange(s.Id, startTime, endTime)

			c.JSON(http.StatusOK, successResponse(&ShortLinkDataAPIResponseData{Histories: rhs}))
			return
		} else if c.Param("action") == "/" {
			handler.GetShortLinkAPI()(c)
			return
		}

		c.JSON(http.StatusOK, errorResponse(4999, "请求资源不存在"))
	})
}
