package handler

import (
	"github.com/jwma/jump-jump/util"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"

	"github.com/jwma/jump-jump/models"
	"github.com/jwma/jump-jump/server/landing_api/service"
)

type UserHandler interface {
	LoginAPI(c *gin.Context)
	GetUserInfoAPI() gin.HandlerFunc
	LogoutAPI() gin.HandlerFunc
	ChangePasswordAPI() gin.HandlerFunc
}

type defaultUserHandler struct {
	service service.User
}

func NewDefaultUser(ms service.User) *defaultUserHandler {
	return &defaultUserHandler{service: ms}
}

type LoginAPIRequest struct {
	Username string `json:"username" binding:"required" example:"your_username"`
	Password string `json:"password" binding:"required" example:"your_password"`
} // @name LoginAPIRequest

type LoginAPIResponseData struct {
	Token string `json:"token,omitempty" example:"xxx.xxx.xxx"`
} // @name LoginAPIResponseData

// LoginAPI godoc
// @Summary 账号登入
// @Description 账号密码登入
// @Tags 账号
// @Accept json
// @Produce json
// @Param body body models.LoginAPIRequest true "登入请求"
// @Success 200 {object} models.Response{data=models.LoginAPIResponseData}
// @Router /user/login [post]
func (handler *defaultUserHandler) LoginAPI(c *gin.Context) {
	req := new(LoginAPIRequest)

	if err := c.ShouldBindJSON(req); err != nil {
		c.JSON(http.StatusOK, errorResponse(4999, "用户名或密码错误"))
		return
	}

	//repo := repository.NewUserRepo(server.store)
	//u, err := repo.FindOneByUsername(strings.TrimSpace(req.Username))
	//if err != nil {
	//	c.JSON(http.StatusOK, errorResponse(4999, "用户名或密码错误"))
	//	return
	//}
	u, err := handler.service.FindOneByUsername(strings.TrimSpace(req.Username))
	if err != nil {
		c.JSON(http.StatusOK, errorResponse(4999, "用户名或密码错误"))
		return
	}

	dk, _ := util.EncodePassword([]byte(req.Password), u.Salt)
	if string(u.Password) != string(dk) {
		c.JSON(http.StatusOK, errorResponse(4999, "用户名或密码错误"))
		return
	}

	c.JSON(http.StatusOK, successResponse(LoginAPIResponseData{
		Token: util.GenerateJWT(u.Username), // 生成 Token 令牌
	}))
}

type GetUserInfoAPIResponseData struct {
	// 用户名
	Username string `json:"username" example:"admin"`

	// 角色，1 普通用户 | 2 管理员
	Role int `json:"role" example:"1" enums:"1,2"`
} // @name GetUserInfoAPIResponseData

// GetUserInfoAPI godoc
// @Security ApiKeyAuth
// @Summary 获取账号信息
// @Description 获取账号信息
// @Tags 账号
// @Accept json
// @Produce json
// @Success 200 {object} models.Response{data=models.GetUserInfoAPIResponseData}
// @Failure 401
// @Router /user/info [get]
func (handler *defaultUserHandler) GetUserInfoAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		c.JSON(http.StatusOK, successResponse(GetUserInfoAPIResponseData{
			Username: user.Username,
			Role:     user.Role,
		}))
	})
}

// LogoutAPI godoc
// @Security ApiKeyAuth
// @Summary 登出
// @Description 登出
// @Tags 账号
// @Accept json
// @Produce json
// @Success 200 {object} models.Response
// @Failure 401
// @Router /user/logout [post]
func (handler *defaultUserHandler) LogoutAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		c.JSON(http.StatusOK, successResponse(nil))
	})
}

type ChangePasswordAPIRequest struct {
	// 原密码
	Password string `json:"password"`

	// 新密码
	NewPassword string `json:"newPassword"`
} // @name ChangePasswordAPIRequest

// ChangePasswordAPI godoc
// @Security ApiKeyAuth
// @Summary 修改账号密码
// @Description 修改账号密码
// @Tags 账号
// @Accept json
// @Produce json
// @Param body body models.ChangePasswordAPIRequest true "修改密码请求"
// @Success 200 {object} models.Response
// @Failure 401
// @Router /user/change-password [post]
func (handler *defaultUserHandler) ChangePasswordAPI() gin.HandlerFunc {
	return Authenticator(func(c *gin.Context, user *models.User) {
		if user.Username == "guest" {
			c.JSON(http.StatusOK, errorResponse(4999, "该账号不支持修改密码"))
			return
		}

		p := &ChangePasswordAPIRequest{}
		if err := c.ShouldBindJSON(p); err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, "请填写原密码和新密码"))
			return
		}

		dk, _ := util.EncodePassword([]byte(p.Password), user.Salt)
		if string(user.Password) != string(dk) {
			c.JSON(http.StatusOK, errorResponse(4999, "原密码错误"))
			return
		}

		user.RawPassword = p.NewPassword

		//repo := repository.NewUserRepo(server.store)
		//err := repo.UpdatePassword(user)
		//if err != nil {
		//	c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
		//	return
		//}
		err := handler.service.UpdatePassword(user)
		if err != nil {
			c.JSON(http.StatusOK, errorResponse(4999, err.Error()))
			return
		}

		c.JSON(http.StatusOK, successResponse(nil))
	})
}
