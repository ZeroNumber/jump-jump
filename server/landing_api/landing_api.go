package landing_api

import (
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"github.com/go-redis/redis"

	"github.com/jwma/jump-jump/server/landing_api/handler"
	"github.com/jwma/jump-jump/server/landing_api/repository"
	"github.com/jwma/jump-jump/server/landing_api/service"
)

type server struct {
	store  *redis.Client
	router *gin.Engine
}

func NewServer(store *redis.Client) *server {
	router := gin.Default()
	server := &server{
		store:  store,
		router: router,
	}

	setupRoute(router)

	userRepository := repository.NewDefaultUser(store)
	userService := service.NewDefaultUser(userRepository)
	userHandler := handler.NewDefaultUser(userService)
	registerUserApi(router, store, userHandler)

	registerConfigApi(router, store)

	shortLinkRepository := repository.NewDefaultShortLinkRepo(store)
	repositoryHistoryRepository := repository.NewDefaultRequestHistory(store)
	shortLinkService := service.NewDefaultShortLink(shortLinkRepository, repositoryHistoryRepository)
	shortLinkHandler := handler.NewDefaultShortLink(shortLinkService)
	registerShortLinkApi(router, store, shortLinkHandler)

	return server
}

func (server *server) Start(addr string) error {
	return server.router.Run(addr)
}

func setupRoute(r *gin.Engine) {
	if gin.Mode() == gin.DebugMode { // 开发环境下，开启 CORS
		corsCfg := cors.DefaultConfig()
		corsCfg.AllowAllOrigins = true
		corsCfg.AddAllowHeaders("Authorization")
		r.Use(cors.New(corsCfg))
	}

	r.Use(handler.AllowedHostsMiddleware())
	r.Use(gzip.Gzip(gzip.DefaultCompression, gzip.WithExcludedPaths([]string{"/v1/"})))

	// serve dashboard static resources
	r.LoadHTMLFiles("./web/admin/index.html")
	r.StaticFS("/static", http.Dir("./web/admin/static"))
	r.GET("/", func(c *gin.Context) {
		c.HTML(http.StatusOK, "index.html", gin.H{})
	})
}

func registerUserApi(r *gin.Engine, store *redis.Client, h handler.UserHandler) {
	v1 := r.Group("/v1")
	{
		v1.POST("/user/login", h.LoginAPI)
		v1.GET("/user/info", handler.JWTAuthenticatorMiddleware(store), h.GetUserInfoAPI())
		v1.POST("/user/logout", handler.JWTAuthenticatorMiddleware(store), h.LogoutAPI())
		v1.POST("/user/change-password", handler.JWTAuthenticatorMiddleware(store), h.ChangePasswordAPI())
	}
}

func registerConfigApi(r *gin.Engine, store *redis.Client) {
	v1 := r.Group("/v1")
	{
		// 系统配置
		v1.GET("/config", handler.JWTAuthenticatorMiddleware(store), handler.GetConfigAPI)
		v1.PATCH("/config/landing-hosts", handler.JWTAuthenticatorMiddleware(store), handler.UpdateLandingHostsAPI())
		v1.PATCH("/config/id-length", handler.JWTAuthenticatorMiddleware(store), handler.UpdateIdLengthConfigAPI())
		v1.PATCH("/config/short-link-404-handling", handler.JWTAuthenticatorMiddleware(store), handler.UpdateShortLinkNotFoundConfigAPI())

	}
}

func registerShortLinkApi(r *gin.Engine, store *redis.Client, h handler.ShortLink) {
	v1 := r.Group("/v1")
	{
		shortLinkAPI := v1.Group("/short-link")
		shortLinkAPI.Use(handler.JWTAuthenticatorMiddleware(store))
		shortLinkAPI.GET("/", h.ListShortLinksAPI())
		shortLinkAPI.GET("/:id", h.GetShortLinkAPI())
		shortLinkAPI.POST("/", h.CreateShortLinkAPI())
		shortLinkAPI.PATCH("/:id", h.UpdateShortLinkAPI())
		shortLinkAPI.DELETE("/:id", h.DeleteShortLinkAPI())
		shortLinkAPI.GET("/:id/*action", h.ShortLinkActionAPI())
	}

	v2 := r.Group("/v2")
	{
		shortLinkAPI := v2.Group("/short-link")
		shortLinkAPI.Use(handler.JWTAuthenticatorMiddleware(store))
		shortLinkAPI.DELETE("/", h.DeleteShortLinkAPIV2())
	}
}

// Swagger
// docs.SwaggerInfo.Title = "Jump Jump API Documentation"
// docs.SwaggerInfo.Description = "🚀🚀🚀"
// docs.SwaggerInfo.Version = "v1"
// docs.SwaggerInfo.BasePath = "/v1"
// docs.SwaggerInfo.Schemes = []string{"http", "https"}
// url := ginSwagger.URL("/swagger/doc.json")
// detectAPIDocHost()
// docsR := r.Group("/swagger", gin.BasicAuth(getAPIDocBasicAccounts()))
// {
// 	docsR.GET("/*any", ginSwagger.WrapHandler(swaggerFiles.Handler, url))
// }
