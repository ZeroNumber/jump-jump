package util

import (
	"github.com/spf13/viper"
	"time"
)

//type Config struct {
//	RedisDB        string `mapstructure:"REDIS_DB"`
//	RedisHost      string `mapstructure:"REDIS_HOST"`
//	RedisPassword  string `mapstructure:"REDIS_PASSWORD"`
//	ApiDocHost     string `mapstructure:"API_DOC_HOST"`
//	ApiDocUsername string `mapstructure:"API_DOC_USERNAME"`
//	ApiDocPassword string `mapstructure:"API_DOC_PASSWORD"`
//	J2ApiAddr      string `mapstructure:"J2_API_ADDR"`
//	J2LandingAddr  string `mapstructure:"J2_LANDING_ADDR"`
//	AllowedHosts   string `mapstructure:"ALLOWED_HOSTS"`
//	SecretKey      string `mapstructure:"SECRET_KEY"`
//	DBDriver       string `mapstructure:"DB_DRIVER"`
//	DBSource       string `mapstructure:"DB_SOURCE"`
//	BotAddr        string `mapstructure:"BOT_ADDR"`
//	BotManageUID   int64  `mapstructure:"BOT_MANAGE_UID"`
//}

type Config struct {
	RedisDB           string        `mapstructure:"redis_db"`
	RedisHost         string        `mapstructure:"redis_host"`
	RedisPassword     string        `mapstructure:"redis_password"`
	ApiDocHost        string        `mapstructure:"api_doc_host"`
	ApiDocUsername    string        `mapstructure:"api_doc_username"`
	ApiDocPassword    string        `mapstructure:"api_doc_password"`
	J2ApiAddr         string        `mapstructure:"j2_api_addr"`
	J2LandingAddr     string        `mapstructure:"j2_landing_addr"`
	AllowedHosts      string        `mapstructure:"allowed_hosts"`
	SecretKey         string        `mapstructure:"secret_key"`
	DBDriver          string        `mapstructure:"db_driver"`
	DBSource          string        `mapstructure:"db_source"`
	BotAddr           string        `mapstructure:"bot_addr"`
	BotManageUID      int64         `mapstructure:"bot_manage_uid"`
	BotTargetGroupUID []int64       `mapstructure:"bot_target_group_uid"`
	BotWatermarkPath  string        `mapstructure:"bot_watermark_img"`
	BotCQHttpAddr     string        `mapstructure:"bot_cq_http_addr"`
	BotOffWayWait     time.Duration `mapstructure:"bot_off_day_wait"`
	BotWorkingDayWait time.Duration `mapstructure:"bot_working_day_wait"`
}

func LoadConfig(path string) (config Config, err error) {
	viper.AddConfigPath(path)
	viper.SetConfigName("config")
	viper.SetConfigType("toml")

	// 绑定环境变量
	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		return
	}

	err = viper.Unmarshal(&config)
	return
}
