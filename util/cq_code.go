package util

import (
	"errors"
	"fmt"
	"regexp"
)

type CQCodeData struct {
	Type  string
	Param map[string]string
}

var matchReg = regexp.MustCompile(`\[CQ:\w+?.*?]`)
var typeReg = regexp.MustCompile(`\[CQ:(\w+)`)
var paramReg = regexp.MustCompile(`,([\w\-.]+?)=([^,\]]+)`)

func CQCodeParse(content string) ([]CQCodeData, error) {
	//content := "[CQ:image,file=file:///Users/september/Downloads/IMG_0480.JPG,id=40000] 测试消息 [CQ:face,id=123] test"
	result := make([]CQCodeData, 0)

	allCqCode := matchReg.FindAllString(content, -1)
	for _, cqCode := range allCqCode {
		// 解析类型
		cqType := typeReg.FindStringSubmatch(cqCode)
		if len(cqType) < 2 {
			return nil, errors.New(fmt.Sprintf("解析 CQ 码失败 (%s)", cqCode))
		}
		// 解析参数
		cqParam := paramReg.FindAllStringSubmatch(cqCode, -1)
		newParam := make(map[string]string, 0)
		for _, param := range cqParam {
			if len(param) < 3 {
				continue
			}
			key := param[1]
			value := param[2]
			newParam[key] = value
		}
		// 添加到结果
		result = append(result, CQCodeData{
			Type:  cqType[1],
			Param: newParam,
		})
	}

	return result, nil
}
