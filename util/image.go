package util

import (
	"fmt"
	"github.com/anthonynsimon/bild/transform"
	imgtype "github.com/shamsher31/goimgtype"
	"image"
	"image/color"
	"image/draw"
	"image/jpeg"
	"image/png"
	"log"
	"os"
)

func AddWatermark(inPath, watermarkPath, outPath string) error {
	imgType, err := imgtype.Get(inPath)
	if err != nil {
		return err
	}

	// 读取背景图片
	bgImgFile, err := os.Open(inPath)
	if err != nil {
		return err
	}
	defer checkErr(bgImgFile.Close)

	var bgImgData image.Image
	switch imgType {
	case "image/jpeg":
		bgImgData, _ = jpeg.Decode(bgImgFile)
	case "image/png":
		bgImgData, _ = png.Decode(bgImgFile)
	default:
		return fmt.Errorf("暂不支持的文件类型(%s)", imgType)
	}

	// 读取水印
	watermark, err := os.Open(watermarkPath)
	if err != nil {
		return err
	}
	defer checkErr(watermark.Close)
	// 编码水印
	watermarkImgData, err := png.Decode(watermark)
	if err != nil {
		return err
	}

	//文字水印
	//fontBytes, err := ioutil.ReadFile("test_assets/font.ttf") //读取字体文件
	//if err != nil {
	//	log.Println(err)
	//}
	//font, err := freetype.ParseFont(fontBytes)
	//if err != nil {
	//	log.Println(err)
	//}

	// 按原图边界（界限）生成新图并写入原图
	b := bgImgData.Bounds()
	m := image.NewNRGBA(b)
	draw.Draw(m, b, bgImgData, image.Point{}, draw.Src)

	// 文字水印样式
	//f := freetype.NewContext()
	//f.SetDPI(72)                                                       // 设置DPI
	//f.SetFont(font)                                                    // 设置字体
	//f.SetFontSize(24)                                                  // 设置字号
	//f.SetClip(b)                                                       // 设置剪裁
	//f.SetDst(m)                                                        // 设置绘制目标
	//f.SetSrc(image.NewUniform(color.RGBA{R: 255, G: 0, B: 0, A: 255})) //设置颜色

	x, y := 0, 0
	offsetX, offsetY := 2, 25 // 水印偏移量（间距）

	// 新建一张图片
	// 作为水印画布
	watermarkCanvasScale := 4
	watermarkCanvas := image.NewNRGBA(image.Rect(0, 0, b.Dx()*watermarkCanvasScale, b.Dy()*watermarkCanvasScale))
	maxX := watermarkCanvas.Bounds().Max.X * watermarkCanvasScale
	maxY := watermarkCanvas.Bounds().Max.Y * watermarkCanvasScale

	for y <= maxY {
		for x <= maxX {
			offset := image.Pt(x, y)
			draw.Draw(watermarkCanvas, watermarkImgData.Bounds().Add(offset), watermarkImgData, image.Point{}, draw.Over)

			//绘制文本
			//pt := freetype.Pt(x, y)
			//newPT, err := f.DrawString("btt", pt)
			//if err != nil {
			//	log.Println("写入文字失败")
			//}

			x += watermarkImgData.Bounds().Dx()
			x += offsetX
		}
		y += watermarkImgData.Bounds().Dy()
		y += offsetY
		x = 0
	}

	// 将水印画布转换为图片
	// 绘制到原图之上
	watermarkImage := image.Image(watermarkCanvas)
	watermarkImage = transform.Rotate(watermarkImage, -40.0, nil)
	mask := image.NewUniform(color.Alpha{A: 30})
	draw.DrawMask(m, watermarkImage.Bounds().Add(image.Pt(-watermarkImage.Bounds().Dx()/2, 0)), watermarkImage,
		image.Point{}, mask, image.Point{X: -100, Y: -100}, draw.Over)

	newImg, err := os.Create(outPath)
	if err != nil {
		return err
	}

	return jpeg.Encode(newImg, m, &jpeg.Options{Quality: 100})
}

func checkErr(f func() error) {
	if err := f(); err != nil {
		log.Println(err)
	}
}
