package util

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestAddWatermark(t *testing.T) {
	err := AddWatermark("test_assets/TUAPI-EEES-CC-1120223106.jpeg", "test_assets/watermark.png", "test_assets/new.jpg")
	require.NoError(t, err)
}
