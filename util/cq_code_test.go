package util

import (
	"github.com/stretchr/testify/require"
	"testing"
)

func TestCQCodeParse(t *testing.T) {
	content := "[CQ:image,file=file:///Users/september/Downloads/IMG_0480.JPG,id=40000] 测试消息 [CQ:face,id=123] test"
	result, err := CQCodeParse(content)
	require.NoError(t, err)
	require.NotEmpty(t, result)

	require.Equal(t, "image", result[0].Type)
	require.Equal(t, "file:///Users/september/Downloads/IMG_0480.JPG", result[0].Param["file"])
	require.Equal(t, "40000", result[0].Param["id"])

	require.Equal(t, "face", result[1].Type)
	require.Equal(t, "123", result[1].Param["id"])
}
