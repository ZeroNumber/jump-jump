package util

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

func CQSendPrivateMsg(botHost string, uid int64, msg string) error {

	urlValue, err := url.Parse(fmt.Sprintf("%s/send_private_msg", botHost))
	if err != nil {
		return err
	}

	params := make(url.Values)
	params.Set("user_id", strconv.FormatInt(uid, 10))
	params.Set("message", msg)
	params.Set("auto_escape", "false")

	//如果参数中有中文参数,这个方法会进行URLEncode
	urlValue.RawQuery = params.Encode()

	_, err = http.Get(urlValue.String())
	if err != nil {
		return err
	}

	return nil
}

func CQSendGroupMsg(botHost string, uid int64, msg string) error {

	urlValue, err := url.Parse(fmt.Sprintf("%s/send_group_msg", botHost))
	if err != nil {
		return err
	}

	params := make(url.Values)
	params.Set("group_id", strconv.FormatInt(uid, 10))
	params.Set("message", msg)
	params.Set("auto_escape", "false")

	//如果参数中有中文参数,这个方法会进行URLEncode
	urlValue.RawQuery = params.Encode()

	_, err = http.Get(urlValue.String())
	if err != nil {
		return err
	}

	return nil
}

type CQGetImageInfoResp struct {
	Data struct {
		File     string `json:"file"`
		Filename string `json:"filename"`
		Size     int    `json:"size"`
		Url      string `json:"url"`
	} `json:"data"`
	Retcode int    `json:"retcode"`
	Status  string `json:"status"`
	Wording string `json:"wording"`
	Msg     string `json:"msg"`
}

func CQGetImageInfo(botHost, fileCacheName string) (*CQGetImageInfoResp, error) {

	urlValue, err := url.Parse(fmt.Sprintf("%s/get_image", botHost))
	if err != nil {
		return nil, err
	}

	params := make(url.Values)
	params.Set("file", fileCacheName)

	//如果参数中有中文参数,这个方法会进行URLEncode
	urlValue.RawQuery = params.Encode()

	resp, err := http.Get(urlValue.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := new(CQGetImageInfoResp)
	err = json.Unmarshal(body, result)
	if err != nil {
		return nil, err
	}

	if result.Retcode != 0 || result.Status != "ok" {
		return nil, errors.New(fmt.Sprintf("获取图片信息失败 [%s]", result.Msg))
	}

	return result, nil
}

type CQGetVersionInfoResp struct {
	Data struct {
		AppFullName              string `json:"app_full_name"`
		AppName                  string `json:"app_name"`
		AppVersion               string `json:"app_version"`
		CoolqDirectory           string `json:"coolq_directory"`
		CoolqEdition             string `json:"coolq_edition"`
		GoCqhttp                 bool   `json:"go-cqhttp"`
		PluginBuildConfiguration string `json:"plugin_build_configuration"`
		PluginBuildNumber        int    `json:"plugin_build_number"`
		PluginVersion            string `json:"plugin_version"`
		Protocol                 int    `json:"protocol"`
		ProtocolVersion          string `json:"protocol_version"`
		RuntimeOs                string `json:"runtime_os"`
		RuntimeVersion           string `json:"runtime_version"`
		Version                  string `json:"version"`
	} `json:"data"`
	Retcode int    `json:"retcode"`
	Status  string `json:"status"`
	Wording string `json:"wording"`
	Msg     string `json:"msg"`
}

func CQGetVersionInfo(botHost string) (*CQGetVersionInfoResp, error) {

	urlValue, err := url.Parse(fmt.Sprintf("%s/get_version_info", botHost))
	if err != nil {
		return nil, err
	}

	resp, err := http.Get(urlValue.String())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	result := new(CQGetVersionInfoResp)
	err = json.Unmarshal(body, result)
	if err != nil {
		return nil, err
	}

	if result.Retcode != 0 || result.Status != "ok" {
		return nil, errors.New(fmt.Sprintf("获取版本信息失败 [%s]", result.Msg))
	}

	return result, nil
}
