package util

import (
	"github.com/stretchr/testify/require"
	"testing"
)

const botHost = "http://localhost:8000"

func TestCQSendMsg(t *testing.T) {
	err := CQSendPrivateMsg(botHost, 2930929415, "[CQ:image,file=88bfb7096353d754a56d9c9da92d977c.image] 测试消息")
	require.NoError(t, err)
}

func TestCQGetImageInfo(t *testing.T) {
	result, err := CQGetImageInfo(botHost, "fcec49bf1d6d8efab5f008c31f8f64f4.image")
	require.NoError(t, err)
	require.NotEmpty(t, result)

	t.Log(result)
}

func TestCQGetVersionInfo(t *testing.T) {
	result, err := CQGetVersionInfo(botHost)
	require.NoError(t, err)
	require.NotEmpty(t, result)

	t.Log(result)
}
