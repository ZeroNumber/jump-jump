package util

import (
	"strconv"

	"github.com/go-redis/redis"
)

//var client *redis.Client

func NewRedisClient(redisDB, redisHost, redisPassword string) *redis.Client {
	dbIdx, err := strconv.Atoi(redisDB)
	if err != nil {
		panic("please set REDIS_DB env")
	}

	//if client == nil {
	//	client = redis.NewClient(&redis.Options{
	//		Addr:        redisHost,
	//		Password:    redisPassword,
	//		DB:          dbIdx,
	//		IdleTimeout: -1,
	//	})
	//}
	return redis.NewClient(&redis.Options{
		Addr:        redisHost,
		Password:    redisPassword,
		DB:          dbIdx,
		IdleTimeout: -1,
	})
}
