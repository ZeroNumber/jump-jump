package main

import (
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/jwma/jump-jump/internal/app/report"
	"github.com/jwma/jump-jump/util"
)

func main() {
	config, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	cache := util.NewRedisClient(config.RedisDB, config.RedisHost, config.RedisPassword)

	// 每 30 秒运行一次报表生成/更新
	rg := report.NewGenerator(cache, time.Second*30)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		_ = rg.Stop() // 如果程序中断，停止 report generator
		os.Exit(1)
	}()

	if err := rg.Start(); err != nil {
		panic(err)
	}
}
