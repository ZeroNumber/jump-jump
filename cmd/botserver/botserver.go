package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jwma/jump-jump/internal/app/config"
	"github.com/jwma/jump-jump/server/bot"
	"github.com/thoas/go-funk"
	"log"

	"github.com/jwma/jump-jump/util"
)

func main() {
	c, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	if c.J2ApiAddr == "" {
		log.Fatal("missing J2_API_ADDR environment variable")
	}

	err = runBotApi(c)
	if err != nil {
		log.Fatal(err)
	}
}

func runBotApi(c util.Config) error {

	cache := util.NewRedisClient(c.RedisDB, c.RedisHost, c.RedisPassword)

	err := allowHostsChecking(c.AllowedHosts)
	if err != nil {
		return err
	}

	err = cache.Ping().Err()
	if err != nil {
		return err
	}

	err = config.SetupConfig(cache)
	if err != nil {
		return err
	}

	serverConfig := &bot.ServerConfig{
		BotCQHttpHost:  c.BotCQHttpAddr,
		UID:            c.BotManageUID,
		TargetGroupUID: c.BotTargetGroupUID,
		WatermarkPath:  c.BotWatermarkPath,
		Store:          cache,
	}
	server := bot.NewServer(serverConfig)
	return server.Start(c.BotAddr, c.BotWorkingDayWait, c.BotOffWayWait)
}

func allowHostsChecking(allowedHosts string) error {
	if gin.Mode() != gin.ReleaseMode {
		return nil
	}
	if funk.ContainsString([]string{"", "*"}, allowedHosts) {
		return fmt.Errorf("please set ALLOWED_HOSTS environment variable when GIN_MODE=release")
	}
	return nil
}
