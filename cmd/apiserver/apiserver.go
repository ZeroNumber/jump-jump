package main

import (
	"fmt"
	"log"

	"github.com/gin-gonic/gin"
	"github.com/thoas/go-funk"

	"github.com/jwma/jump-jump/internal/app/config"
	"github.com/jwma/jump-jump/server/landing_api"
	"github.com/jwma/jump-jump/util"
)

func main() {
	c, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	if c.J2ApiAddr == "" {
		log.Fatal("missing J2_API_ADDR environment variable")
	}

	err = runLandingApi(c.J2ApiAddr, c.AllowedHosts, c.RedisDB, c.RedisHost, c.RedisPassword)
	if err != nil {
		log.Fatal(err)
	}
}

func runLandingApi(addr, allowedHosts, redisDB, redisHost, redisPassword string) error {

	cache := util.NewRedisClient(redisDB, redisHost, redisPassword)

	err := allowHostsChecking(allowedHosts)
	if err != nil {
		return err
	}

	err = cache.Ping().Err()
	if err != nil {
		return err
	}

	err = config.SetupConfig(cache)
	if err != nil {
		return err
	}

	server := landing_api.NewServer(cache)
	return server.Start(addr)
}

func allowHostsChecking(allowedHosts string) error {
	if gin.Mode() != gin.ReleaseMode {
		return nil
	}
	if funk.ContainsString([]string{"", "*"}, allowedHosts) {
		return fmt.Errorf("please set ALLOWED_HOSTS environment variable when GIN_MODE=release")
	}
	return nil
}
