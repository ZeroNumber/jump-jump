package main

import (
	"github.com/jwma/jump-jump/internal/app/config"
	"github.com/jwma/jump-jump/server/landing"
	"log"

	"github.com/jwma/jump-jump/util"
)

func main() {
	c, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	if c.J2ApiAddr == "" {
		log.Fatal("missing J2_API_ADDR environment variable")
	}

	err = runLanding(c.J2LandingAddr, c.RedisDB, c.RedisHost, c.RedisPassword)
	if err != nil {
		log.Fatal(err)
	}
}

func runLanding(addr, redisDB, redisHost, redisPassword string) error {

	cache := util.NewRedisClient(redisDB, redisHost, redisPassword)

	err := cache.Ping().Err()
	if err != nil {
		return err
	}

	err = config.SetupConfig(cache)
	if err != nil {
		return err
	}

	server := landing.NewServer(cache)
	return server.Start(addr)
}
