package main

import (
	"flag"
	"github.com/jwma/jump-jump/util"
	"log"
	"strings"

	"github.com/jwma/jump-jump/internal/app/repository"
	"github.com/jwma/jump-jump/models"
)

func main() {
	username := flag.String("username", "", "username.")
	password := flag.String("password", "", "password.")
	role := flag.Int("role", models.RoleUser, "role, 1: normal user, 2: administrator.")
	flag.Parse()

	user := &models.User{
		Username:    strings.TrimSpace(*username),
		RawPassword: strings.TrimSpace(*password),
		Role:        *role,
	}

	c, err := util.LoadConfig(".")
	if err != nil {
		log.Fatal("cannot load config:", err)
	}

	repo := repository.NewUserRepo(util.NewRedisClient(c.RedisDB, c.RedisHost, c.RedisPassword))
	err = repo.Save(user)
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("create user %s successfully\n", *username)
}
