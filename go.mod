module github.com/jwma/jump-jump

go 1.13

require (
	github.com/alecthomas/template v0.0.0-20190718012654-fb15b899a751
	github.com/anthonynsimon/bild v0.13.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-gonic/gin v1.7.7
	github.com/go-openapi/spec v0.20.3 // indirect
	github.com/go-redis/redis v6.15.7+incompatible
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/golang/mock v1.6.0
	github.com/jwma/reborn v0.0.0-20200326132007-2fa07c258aec
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/lib/pq v1.10.5
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/mssola/user_agent v0.5.2
	github.com/onsi/ginkgo v1.12.0 // indirect
	github.com/onsi/gomega v1.9.0 // indirect
	github.com/shamsher31/goimgext v1.0.0 // indirect
	github.com/shamsher31/goimgtype v1.0.0
	github.com/spf13/viper v1.11.0
	github.com/stretchr/testify v1.7.1
	github.com/swaggo/swag v1.7.0
	github.com/thoas/go-funk v0.6.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	golang.org/x/image v0.0.0-20210216034530-4410531fe030
)
