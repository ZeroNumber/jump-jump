build_image:
	@echo "building Jump Jump docker image..."
	docker build -t joyde68/jump-jump:master -f build/package/Dockerfile .

docs:
	swag init -g ./internal/app/routers/router.go

run:
	docker-compose -f deployments/docker-compose.yaml -p jumpjump up -d

stop:
	docker-compose -f deployments/docker-compose.yaml stop

build:
	go build -o build/apiserver ./cmd/apiserver
	go build -o build/landingserver ./cmd/landingserver
	go build -o build/createuser ./cmd/createuser
	go build -o build/migraterequesthistory ./cmd/migraterequesthistory
	go build -o build/reportgenerator ./cmd/reportgenerator
	go build -o build/botserver ./cmd/botserver

createpostgres:
	docker run --name postgres12 -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=secret -d postgres:12-alpine

removepostgres:
	docker stop postgres12
	docker rm postgres12

createdb:
	docker exec -it postgres12 createdb --username=root --owner=root simple_bank

dropdb:
	docker exec -it postgres12 dropdb simple_bank

dbinit:
	 migrate create -ext sql -dir db/migration -seq init_schema

dbup:
	migrate -path db/migration -database "postgresql://root:secret@localhost:5432/simple_bank?sslmode=disable" -verbose up

dbdown:
	migrate -path db/migration -database "postgresql://root:secret@localhost:5432/simple_bank?sslmode=disable" -verbose down

sqlc:
	sqlc generate

test:
	go test -v -cover ./...

mock:
	mockgen -package mockdb -destination db/mock/store.go github.com/jwma/jump-jump/db/sqlc Store

.PHONY: build