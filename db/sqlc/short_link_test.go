package db

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/jwma/jump-jump/util"
	"github.com/stretchr/testify/require"
)

func createRandomShortLink(t *testing.T) ShortLink {
	user := createRandomUser(t)

	arg := CreateShortLinkParams{
		Key:      util.RandomString(6),
		ToUrl:    util.RandomString(12),
		OwnerID:  user.ID,
		IsEnable: util.RandomBool(),
	}

	shortLink, err := testQueries.CreateShortLink(context.TODO(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, shortLink)

	require.Equal(t, arg.Key, shortLink.Key)
	require.Equal(t, arg.ToUrl, shortLink.ToUrl)
	require.Equal(t, arg.OwnerID, shortLink.OwnerID)
	require.Equal(t, arg.IsEnable, shortLink.IsEnable)

	require.NotZero(t, shortLink.ID)
	require.NotZero(t, shortLink.CreateTime)

	return shortLink
}

func TestCreateShortLink(t *testing.T) {
	createRandomShortLink(t)
}

func TestFirstShortLink(t *testing.T) {
	shortLink1 := createRandomShortLink(t)
	shortLink2, err := testQueries.FirstShortLinkByKey(context.TODO(), shortLink1.Key)
	require.NoError(t, err)
	require.NotEmpty(t, shortLink2)

	require.Equal(t, shortLink1.ID, shortLink2.ID)
	require.Equal(t, shortLink1.Key, shortLink2.Key)
	require.Equal(t, shortLink1.ToUrl, shortLink2.ToUrl)
	require.Equal(t, shortLink1.Description, shortLink2.Description)
	require.Equal(t, shortLink1.OwnerID, shortLink2.OwnerID)
	require.Equal(t, shortLink1.IsEnable, shortLink2.IsEnable)

	require.WithinDuration(t, shortLink1.CreateTime, shortLink2.CreateTime, time.Second) // 检测时间差
	require.WithinDuration(t, shortLink1.UpdateTime, shortLink2.UpdateTime, time.Second)
}

func TestUpdateShortLinkToUrl(t *testing.T) {
	shortLink1 := createRandomShortLink(t)

	arg := UpdateShortLinkToUrlParams{
		ID:         shortLink1.ID,
		ToUrl:      util.RandomString(12),
		UpdateTime: time.Now(),
	}
	shortLink2, err := testQueries.UpdateShortLinkToUrl(context.TODO(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, shortLink2)

	require.Equal(t, shortLink1.ID, shortLink2.ID)
	require.Equal(t, shortLink1.Key, shortLink2.Key)
	require.Equal(t, arg.ToUrl, shortLink2.ToUrl) // 更新项
	require.Equal(t, shortLink1.Description, shortLink2.Description)
	require.Equal(t, shortLink1.OwnerID, shortLink2.OwnerID)
	require.Equal(t, shortLink1.IsEnable, shortLink2.IsEnable)

	require.WithinDuration(t, shortLink1.CreateTime, shortLink2.CreateTime, time.Second) // 检测时间差
	require.WithinDuration(t, arg.UpdateTime, shortLink2.UpdateTime, time.Second)
}

func TestDeleteShortLink(t *testing.T) {
	shortLink1 := createRandomShortLink(t)

	err := testQueries.DeleteShortLink(context.TODO(), shortLink1.ID)
	require.NoError(t, err)

	shortLink2, err := testQueries.FirstShortLinkByKey(context.TODO(), shortLink1.Key)
	require.Error(t, err)
	require.EqualError(t, err, sql.ErrNoRows.Error())
	require.Empty(t, shortLink2)
}

func TestGetShortLink(t *testing.T) {
	for i := 0; i < 10; i++ {
		createRandomShortLink(t)
	}

	arg := GetShortLinkParams{
		Limit:  5,
		Offset: 5,
	}

	shortLinks, err := testQueries.GetShortLink(context.TODO(), arg)
	require.NoError(t, err)
	require.Len(t, shortLinks, 5)

	for _, shortLink := range shortLinks {
		require.NotEmpty(t, shortLink)
	}
}
