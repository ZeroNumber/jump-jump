package db

import (
	"context"
	"database/sql"
	"testing"
	"time"

	"github.com/jwma/jump-jump/util"
	"github.com/stretchr/testify/require"
)

func createRandomUser(t *testing.T) User {
	arg := CreateUserParams{
		Username: util.RandomString(6),
		Password: util.RandomString(12),
		Role:     int16(util.RandomInt(1, 10)),
	}

	user, err := testQueries.CreateUser(context.TODO(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, user)

	require.Equal(t, arg.Username, user.Username)
	require.Equal(t, arg.Password, user.Password)
	require.Equal(t, arg.Role, user.Role)

	require.NotZero(t, user.ID)
	require.NotZero(t, user.CreateTime)

	return user
}

func TestCreateUser(t *testing.T) {
	createRandomUser(t)
}

func TestFirstUser(t *testing.T) {
	user1 := createRandomUser(t)
	user2, err := testQueries.FirstUserByUsername(context.TODO(), user1.Username)
	require.NoError(t, err)
	require.NotEmpty(t, user2)

	require.Equal(t, user1.ID, user2.ID)
	require.Equal(t, user1.Username, user2.Username)
	require.Equal(t, user1.Role, user2.Role)
	require.Equal(t, user1.Password, user2.Password)

	require.WithinDuration(t, user1.CreateTime, user2.CreateTime, time.Second) // 检测时间差
	require.WithinDuration(t, user1.UpdateTime, user2.UpdateTime, time.Second)
}

func TestUpdateUserToUrl(t *testing.T) {
	user1 := createRandomUser(t)

	arg := UpdateUserPasswordParams{
		ID:         user1.ID,
		Password:   util.RandomString(12),
		UpdateTime: time.Now(),
	}
	user2, err := testQueries.UpdateUserPassword(context.TODO(), arg)
	require.NoError(t, err)
	require.NotEmpty(t, user2)

	require.Equal(t, user1.ID, user2.ID)
	require.Equal(t, user1.Username, user2.Username)
	require.Equal(t, user1.Role, user2.Role)
	require.Equal(t, arg.Password, user2.Password) // 更新项

	require.WithinDuration(t, user1.CreateTime, user2.CreateTime, time.Second) // 检测时间差
	require.WithinDuration(t, arg.UpdateTime, user2.UpdateTime, time.Second)
}

func TestDeleteUser(t *testing.T) {
	user1 := createRandomUser(t)

	err := testQueries.DeleteUser(context.TODO(), user1.ID)
	require.NoError(t, err)

	user2, err := testQueries.FirstUserByUsername(context.TODO(), user1.Username)
	require.Error(t, err)
	require.EqualError(t, err, sql.ErrNoRows.Error())
	require.Empty(t, user2)
}

func TestGetUser(t *testing.T) {
	for i := 0; i < 10; i++ {
		createRandomUser(t)
	}

	arg := GetUserParams{
		Limit:  5,
		Offset: 5,
	}

	users, err := testQueries.GetUser(context.TODO(), arg)
	require.NoError(t, err)
	require.Len(t, users, 5)

	for _, user := range users {
		require.NotEmpty(t, user)
	}
}
