-- name: CreateShortLink :one
INSERT INTO "short_link" (
    "key", "to_url", "description", "owner_id", "is_enable"
) VALUES (
    $1, $2, $3, $4, $5
)
RETURNING *;

-- name: FirstShortLinkByKey :one
SELECT * FROM "short_link"
WHERE "key" = $1
LIMIT 1;

-- name: GetShortLink :many
SELECT * FROM "short_link"
ORDER by "create_time"
LIMIT $1
OFFSET $2;

-- name: UpdateShortLinkToUrl :one
UPDATE "short_link"
SET "to_url" = $1, "update_time" = $2
WHERE "id" = $3
RETURNING *;

-- name: DeleteShortLink :exec
DELETE FROM "short_link" WHERE "id" = $1;
