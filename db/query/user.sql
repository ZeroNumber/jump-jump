-- name: CreateUser :one
INSERT INTO "user" (
    "username", "password", "role"
) VALUES (
    $1, $2, $3
)
RETURNING *;

-- name: FirstUserByUsername :one
SELECT * FROM "user"
WHERE "username" = $1
LIMIT 1;

-- name: GetUser :many
SELECT * FROM "user"
ORDER by "create_time"
LIMIT $1
OFFSET $2;

-- name: UpdateUserPassword :one
UPDATE "user"
SET "password" = $1, "update_time" = $2
WHERE "id" = $3
RETURNING *;

-- name: DeleteUser :exec
DELETE FROM "user" WHERE "id" = $1;
