CREATE TABLE "user" (
  "id" bigserial PRIMARY KEY,
  "username" varchar UNIQUE NOT NULL,
  "role" smallint NOT NULL,
  "password" varchar NOT NULL,
  "create_time" timestamptz NOT NULL DEFAULT (now()),
  "update_time" timestamptz NOT NULL DEFAULT (now())
);

CREATE TABLE "short_link" (
  "id" bigserial PRIMARY KEY,
  "owner_id" bigint NOT NULL,
  "key" varchar UNIQUE NOT NULL,
  "to_url" varchar NOT NULL,
  "description" varchar NOT NULL DEFAULT '',
  "is_enable" bool NOT NULL,
  "create_time" timestamptz NOT NULL DEFAULT (now()),
  "update_time" timestamptz NOT NULL DEFAULT (now())
);

ALTER TABLE "short_link" ADD FOREIGN KEY ("owner_id") REFERENCES "user" ("id");

CREATE INDEX ON "short_link" ("owner_id");

CREATE INDEX ON "short_link" ("is_enable");

CREATE INDEX ON "short_link" ("owner_id", "is_enable");

COMMENT ON COLUMN "short_link"."owner_id" IS '所有者';

COMMENT ON COLUMN "short_link"."to_url" IS '目标链接';
