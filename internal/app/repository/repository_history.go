package repository

import (
	"encoding/json"
	"github.com/jwma/jump-jump/util"
	"log"
	"strconv"
	"time"

	"github.com/go-redis/redis"

	"github.com/jwma/jump-jump/models"
)

type requestHistoryRepository struct {
	db *redis.Client
}

//var requestHistoryRepo *requestHistoryRepository

func NewRequestHistoryRepo(rdb *redis.Client) *requestHistoryRepository {
	//if requestHistoryRepo == nil {
	//	requestHistoryRepo = &requestHistoryRepository{rdb}
	//}
	return &requestHistoryRepository{rdb}
}

func (r *requestHistoryRepository) Save(rh *models.RequestHistory) {
	rh.Id = util.RandStringRunes(6)
	rh.Time = time.Now()
	key := util.GetRequestHistoryKey(rh.Link.Id)

	_, err := r.db.ZAdd(key, redis.Z{
		Score:  float64(rh.Time.Unix()),
		Member: rh,
	}).Result()

	if err != nil {
		log.Printf("fail to save request history with key: %s, error: %v, data: %v\n", key, err, rh)
	}
}

func (r *requestHistoryRepository) FindLatest(linkId string, size int64) (*requestHistoryListResult, error) {
	key := util.GetRequestHistoryKey(linkId)
	rs, err := r.db.ZRangeWithScores(key, -size, -1).Result()

	if err != nil {
		log.Printf("failed to find request history latest records with key: %s, err: %v\n", key, err)
	}

	util.ReverseAny(rs)
	result := newEmptyRequestHistoryResult()
	for _, one := range rs {
		rh := &models.RequestHistory{}
		_ = json.Unmarshal([]byte(one.Member.(string)), rh)
		result.addHistory(rh)
	}

	return result, nil
}

func (r *requestHistoryRepository) FindByDateRange(linkId string, startTime, endTime time.Time) []*models.RequestHistory {
	rs, _ := r.db.ZRangeByScoreWithScores(util.GetRequestHistoryKey(linkId), redis.ZRangeBy{
		Min: strconv.Itoa(int(startTime.Unix())),
		Max: strconv.Itoa(int(endTime.Unix())),
	}).Result()
	rhs := make([]*models.RequestHistory, 0)

	for _, one := range rs {
		rh := &models.RequestHistory{}
		_ = json.Unmarshal([]byte(one.Member.(string)), rh)

		rhs = append(rhs, rh)
	}

	return rhs
}
