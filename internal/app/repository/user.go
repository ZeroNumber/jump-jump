package repository

import (
	"encoding/json"
	"fmt"
	"github.com/jwma/jump-jump/util"
	"log"
	"time"

	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/models"
)

type userRepository struct {
	db *redis.Client
}

func NewUserRepo(rdb *redis.Client) *userRepository {
	return &userRepository{rdb}
}

func (r *userRepository) IsExists(username string) bool {
	if username == "" {
		return false
	}

	exists, err := r.db.HExists(util.GetUserKey(), username).Result()
	if err != nil {
		log.Printf("fail to check user exists with username: %s, error: %v\n", username, err)
		return false
	}

	return exists
}

func (r *userRepository) Save(u *models.User) error {
	if u.Username == "" || u.RawPassword == "" {
		return fmt.Errorf("username or password can not be empty string")
	}
	if _, exists := models.Roles[u.Role]; !exists {
		return fmt.Errorf("user role can not be %b", u.Role)
	}
	if r.IsExists(u.Username) {
		return fmt.Errorf("%s already exitis", u.Username)
	}

	salt, _ := util.RandomSalt(32)
	dk, _ := util.EncodePassword([]byte(u.RawPassword), salt)
	u.Password = dk
	u.Salt = salt
	u.CreateTime = time.Now()

	j, _ := json.Marshal(u)
	r.db.HSet(util.GetUserKey(), u.Username, j)
	return nil
}

func (r *userRepository) UpdatePassword(u *models.User) error {
	if u.RawPassword == "" {
		return fmt.Errorf("password can not be empty string")
	}

	salt, _ := util.RandomSalt(32)
	dk, _ := util.EncodePassword([]byte(u.RawPassword), salt)
	u.Password = dk
	u.Salt = salt

	j, _ := json.Marshal(u)
	r.db.HSet(util.GetUserKey(), u.Username, j)
	return nil
}

func (r *userRepository) FindOneByUsername(username string) (*models.User, error) {
	if username == "" {
		return nil, fmt.Errorf("username can not be empty string")
	}

	j, err := r.db.HGet(util.GetUserKey(), username).Result()
	if err != nil {
		log.Printf("fail to get user with username: %s, error: %v\n", username, err)
		return nil, fmt.Errorf("用户不存在")
	}

	u := &models.User{}
	err = json.Unmarshal([]byte(j), u)
	if err != nil {
		log.Printf("fail to Unmarshal user with username: %s, error: %v\n", username, err)
		return nil, fmt.Errorf("用户不存在")
	}
	return u, nil
}
