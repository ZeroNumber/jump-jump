package repository

import (
	"github.com/jwma/jump-jump/util"
	"strconv"
	"time"

	"github.com/go-redis/redis"
	"github.com/jwma/jump-jump/models"
)

type activeLinkRepository struct {
	db *redis.Client
}

//var activeLinkRepo *activeLinkRepository

func NewActiveLinkRepo(rdb *redis.Client) *activeLinkRepository {
	//if activeLinkRepo == nil {
	//	activeLinkRepo = &activeLinkRepository{rdb}
	//}
	return &activeLinkRepository{rdb}
}

func (r *activeLinkRepository) Save(linkId string) {
	r.db.ZAdd(util.GetActiveLinkKey(), redis.Z{
		Score:  float64(time.Now().Unix()),
		Member: linkId,
	})
}

func (r *activeLinkRepository) FindByDateRange(startTime, endTime time.Time) []*models.ActiveLink {
	result := make([]*models.ActiveLink, 0)
	rs, _ := r.db.ZRangeByScoreWithScores(util.GetActiveLinkKey(), redis.ZRangeBy{
		Min: strconv.Itoa(int(startTime.Unix())),
		Max: strconv.Itoa(int(endTime.Unix())),
	}).Result()

	for _, one := range rs {
		result = append(result, &models.ActiveLink{Id: one.Member.(string), Time: time.Unix(int64(one.Score), 0)})
	}

	return result
}
