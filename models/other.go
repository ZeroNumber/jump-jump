package models

import (
	"github.com/jwma/jump-jump/internal/app/config"
)

type GetConfigAPIResponseData struct {
	Config *config.SystemConfig `json:"config"`
} // @name GetConfigAPIResponseData

type UpdateLandingHostsAPIRequest struct {
	Hosts []string `json:"hosts" format:"array" example:"https://a.com/,https://b.com/"`
} // @name UpdateLandingHostsAPIRequest

func ToShortLinkData(s *ShortLink) *ShortLinkData {
	return &ShortLinkData{
		Id:          s.Id,
		Url:         s.Url,
		Description: s.Description,
		IsEnable:    s.IsEnable,
		CreatedBy:   s.CreatedBy,
		CreateTime:  s.CreateTime,
		UpdateTime:  s.UpdateTime,
	}
}

func ToShortLinkDataSlice(s []*ShortLink) []*ShortLinkData {
	r := make([]*ShortLinkData, 0)
	for _, ss := range s {
		r = append(r, ToShortLinkData(ss))
	}
	return r
}
