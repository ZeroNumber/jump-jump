package models

type Message struct {
	Topic string // 消息的主题
	Body  []byte // 消息的Body
}
