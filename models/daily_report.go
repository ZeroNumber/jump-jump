package models

import "encoding/json"

type DailyReport struct {
	PV int            `json:"pv"`
	UV int            `json:"uv"`
	OS map[string]int `json:"os"`
}

func (d *DailyReport) MarshalBinary() (data []byte, err error) {
	return json.Marshal(d)
}

type DailyReportItem struct {
	Date   string       `json:"date"`
	Report *DailyReport `json:"report"`
}
