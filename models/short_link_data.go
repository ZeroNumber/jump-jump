package models

import "time"

type ShortLinkData struct {
	Id string `json:"id" example:"RANDOM_ID" format:"string"`
	// 目标链接
	Url string `json:"url" example:"https://github.com/jwma/jump-jump" format:"string"`
	// 描述
	Description string `json:"description" example:"Jump Jump project" format:"string"`
	// 是否启用
	IsEnable bool `json:"isEnable" example:"true" format:"boolean"`
	// 创建者
	CreatedBy string `json:"createdBy" example:"admin" format:"string"`
	// 创建时间
	CreateTime time.Time `json:"createTime"`
	// 最后更新时间
	UpdateTime time.Time `json:"updateTime"`
} // @name ShortLinkData
