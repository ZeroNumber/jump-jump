package models

import "time"

type ActiveLink struct {
	Id   string
	Time time.Time
}
